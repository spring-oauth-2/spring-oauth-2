package com.thearaseng.security.oauth2.client.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

import java.util.Arrays;

/**
 * @author Theara Seng
 * created on May 08, 2018
 */

@Configuration
@EnableOAuth2Client
public class ClientServerConfig {

    @Autowired
    private OAuth2ClientContext oAuth2ClientContext;

    @Autowired
    private OAuth2ProtectedResourceDetails oAuth2ProtectedResourceDetails;

    @Bean
    public OAuth2RestTemplate oAuth2RestTemplate() {
        return new OAuth2RestTemplate(oAuth2ProtectedResourceDetails, oAuth2ClientContext);
    }

    @Bean
    public OAuth2ProtectedResourceDetails oAuth2ProtectedResourceDetails() {

        AuthorizationCodeResourceDetails details = new AuthorizationCodeResourceDetails();
        details.setClientId("client-1");
        details.setClientSecret("client-1-secret");
        details.setUserAuthorizationUri("http://localhost:8080/oauth/authorize");
        details.setAccessTokenUri("http://localhost:8080/oauth/token");
        details.setScope(Arrays.asList("read", "write", "trust", "public_profile"));

        return details;
    }

}
