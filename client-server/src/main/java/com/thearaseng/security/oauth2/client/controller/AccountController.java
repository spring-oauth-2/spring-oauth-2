package com.thearaseng.security.oauth2.client.controller;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Theara Seng
 * created on May 09, 2018
 */

@RestController
@RequestMapping("/")
public class AccountController {

    @Autowired
    private OAuth2RestTemplate oAuth2RestTemplate;

    @GetMapping
    public JsonNode userProfile() {
        return oAuth2RestTemplate.getForObject("http://localhost:8081/api/me", JsonNode.class);
    }

}
