package com.thearaseng.security.oauth2.service.service;

import com.thearaseng.security.oauth2.service.model.Message;

public interface MessageService {
    Message save(Message message);
}
