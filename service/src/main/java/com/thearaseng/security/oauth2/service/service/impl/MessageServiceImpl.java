package com.thearaseng.security.oauth2.service.service.impl;

import com.thearaseng.security.oauth2.service.model.Message;
import com.thearaseng.security.oauth2.service.repository.MessageRepository;
import com.thearaseng.security.oauth2.service.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;

public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageRepository messageRepository;

    @Override
    public Message save(Message message) {
        return messageRepository.save(message);
    }

}
