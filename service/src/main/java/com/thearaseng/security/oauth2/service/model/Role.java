package com.thearaseng.security.oauth2.service.model;

/**
 * @author Theara Seng
 * created at Feb 27, 2018
 */

public enum Role {
    ADMIN, USER
}
