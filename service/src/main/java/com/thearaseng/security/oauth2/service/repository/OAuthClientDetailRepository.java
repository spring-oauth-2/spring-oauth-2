package com.thearaseng.security.oauth2.service.repository;

import com.thearaseng.security.oauth2.service.model.OAuthClientDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OAuthClientDetailRepository extends JpaRepository<OAuthClientDetail, String> {

}
