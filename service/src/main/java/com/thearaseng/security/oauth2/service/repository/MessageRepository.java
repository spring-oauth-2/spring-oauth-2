package com.thearaseng.security.oauth2.service.repository;

import com.thearaseng.security.oauth2.service.model.Message;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Theara Seng
 */

@Repository
public interface MessageRepository extends CrudRepository<Message, Long> {

}
