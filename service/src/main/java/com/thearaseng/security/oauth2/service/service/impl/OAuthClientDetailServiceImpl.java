package com.thearaseng.security.oauth2.service.service.impl;

import com.thearaseng.security.oauth2.service.model.OAuthClientDetail;
import com.thearaseng.security.oauth2.service.repository.OAuthClientDetailRepository;
import com.thearaseng.security.oauth2.service.service.OAuthClientDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OAuthClientDetailServiceImpl implements OAuthClientDetailService {

    @Autowired
    private OAuthClientDetailRepository oAuthClientDetailRepository;

    @Override
    public OAuthClientDetail save(OAuthClientDetail oAuthClientDetail) {
        return oAuthClientDetailRepository.save(oAuthClientDetail);
    }

}
