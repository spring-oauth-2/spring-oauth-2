package com.thearaseng.security.oauth2.service.service;


import com.thearaseng.security.oauth2.service.model.User;

/**
 * @author Theara Seng
 * Created on Mar 01, 2018
 */

public interface UserService {

    User findByEmail(String email);
    User findById(Long id);
    User save(User user);

}
