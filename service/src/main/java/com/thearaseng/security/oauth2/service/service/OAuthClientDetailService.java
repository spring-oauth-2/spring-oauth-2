package com.thearaseng.security.oauth2.service.service;

import com.thearaseng.security.oauth2.service.model.OAuthClientDetail;

public interface OAuthClientDetailService {

    OAuthClientDetail save(OAuthClientDetail oAuthClientDetail);

}
