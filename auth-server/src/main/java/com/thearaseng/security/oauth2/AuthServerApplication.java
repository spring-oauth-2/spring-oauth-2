package com.thearaseng.security.oauth2;

import com.thearaseng.security.oauth2.service.model.OAuthClientDetail;
import com.thearaseng.security.oauth2.service.model.Role;
import com.thearaseng.security.oauth2.service.model.User;
import com.thearaseng.security.oauth2.service.service.OAuthClientDetailService;
import com.thearaseng.security.oauth2.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthServerApplication implements ApplicationRunner {

    @Autowired
    private OAuthClientDetailService oAuthClientDetailService;

    @Autowired
    private UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(AuthServerApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {

        OAuthClientDetail resourceServer1 = new OAuthClientDetail();
        resourceServer1.setClientId("resource-server-1");
        resourceServer1.setClientSecret("resource-server-1-secret");
        //resourceServer1.setResourceIds("resource-server-1");
        //resourceServer1.setScope("read,write,public_profile");
        //resourceServer1.setAuthorizedGrantTypes("authorization_code,password,client_credentials,refresh_token");
        //resourceServer1.setWebServerRedirectUri("http://localhost:8082/oauth/callback");
        resourceServer1.setAuthorities("ROLE_TOKEN_CHECK");
        //resourceServer1.setAccessTokenValidity(120);
        //resourceServer1.setRefreshTokenValidity(600);
        //client1.setAdditionalInformation("client 1 additional information");
        //resourceServer1.setAutoApprove("true");
        oAuthClientDetailService.save(resourceServer1);

        OAuthClientDetail client1 = new OAuthClientDetail();
        client1.setClientId("client-1");
        client1.setClientSecret("client-1-secret");
        client1.setResourceIds("resource-server-1,resource-server-2");
        client1.setScope("read,write,public_profile");
        client1.setAuthorizedGrantTypes("authorization_code,password,client_credentials,refresh_token");
        client1.setWebServerRedirectUri("http://localhost:8082/oauth/callback");
        client1.setAuthorities("CLIENT");
        client1.setAccessTokenValidity(30);
        client1.setRefreshTokenValidity(600);
        //client1.setAdditionalInformation("client 1 additional information");
        client1.setAutoApprove("true");

        oAuthClientDetailService.save(client1);

        User user = new User();
        user.setEmail("dara@gmail.com");
        user.setPassword("123");
        user.setRole(Role.USER);

        userService.save(user);

    }
}
