package com.thearaseng.security.oauth2.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private TokenStore tokenStore;

    @Bean
    public TokenStore tokenStore() {
        // *** In Memory Token Store ***
        //return new InMemoryTokenStore();

        // *** Jdbc Token Store ***
        return new JdbcTokenStore(dataSource);
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        // *** In Memory Client ***
        /*
        clients.inMemory()

                .withClient("resource-server-1")
                .secret("resource-server-1-secret")
                .authorities("ROLE_TOKEN_CHECK")

                .and()
                .withClient("client-1")
                .secret("client-1-secret")
                .scopes("read", "write", "trust", "public_profile")
                .authorizedGrantTypes("authorization_code", "password", "refresh_token")
                .authorities("ROLE_ADMIN", "ROLE_USER")
                .accessTokenValiditySeconds(60)
                .refreshTokenValiditySeconds(120)
                .and()
                .withClient("client-2")
                .secret("client-2-secret")
                .scopes("read")
                .authorizedGrantTypes("password")
                .authorities("ROLE_USER")
                .accessTokenValiditySeconds(60);
        */

        // *** Store Client Detail to DB ***
        clients.jdbc(dataSource);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .tokenStore(tokenStore)
                .authenticationManager(authenticationManager)
                .userDetailsService(userDetailsService);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {

        security
                .checkTokenAccess("hasRole('ROLE_TOKEN_CHECK')");

    }
}
