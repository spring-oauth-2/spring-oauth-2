package com.thearaseng.security.oauth.resource.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/me")
public class UserController {

    @PreAuthorize("#oauth2.hasScope('public_profile')")
    @GetMapping
    public Map<String, String> me() {
        Map<String, String> response = new HashMap<>();
        response.put("message", "you are accessing private data");
        response.put("value", "very private data");

        return response;
    }

}
